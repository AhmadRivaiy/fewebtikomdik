import React, { Component } from 'react';
import {
  MDBNavbar,
  MDBNavbarBrand,
  MDBNavbarNav,
  MDBNavbarToggler,
  MDBCollapse,
  MDBNavItem,
  MDBFooter,
  MDBNavLink,
  MDBTooltip,
  MDBIcon, MDBView,
  MDBRow, MDBCol, MDBContainer
} from 'mdbreact';
import { BrowserRouter as Router } from 'react-router-dom';
//import logoTikom from './assets/logo_tikomdik_white.png';
import Routes from './Routes';
import Sidebar from "react-sidebar";

class App extends Component {
  state = {
    collapseID: '',
    sidebarOpen: false
  };

  toggleCollapse = collapseID => () =>
    this.setState(prevState => ({
      collapseID: prevState.collapseID !== collapseID ? collapseID : ''
    }));

  closeCollapse = collID => () => {
    const { collapseID } = this.state;
    window.scrollTo(0, 0);
    collapseID === collID && this.setState({ collapseID: '' });
  };

  onSetSidebarOpen(open) {
    this.setState({ sidebarOpen: open });
  }
  
  render() {
    const overlay = (
      <div
        id='sidenav-overlay'
        style={{ backgroundColor: 'transparent' }}
        onClick={this.toggleCollapse('mainNavbarCollapse')}
      />
    );

    const { collapseID, sidebarOpen } = this.state;

    return (
      <Router>
            <div className='flyout'>
          {/* <Sidebar
            sidebar={<b>Sidebar content</b>}
            open={sidebarOpen}
            onSetOpen={() => this.onSetSidebarOpen(!sidebarOpen)}
            touch={true}
            styles={{ sidebar: { background: "white" } }}
          > */}
              <MDBNavbar color='indigo' dark expand='md' fixed='top' scrolling>
                <MDBNavbarToggler
                  onClick={this.toggleCollapse('mainNavbarCollapse')}
                  //onClick={() => this.setState({ sidebarOpen: !sidebarOpen })}
                  right={true}
                  image="https://www.iconfinder.com/data/icons/basic-ui-27/64/Artboard_18-512.png"
                />
                <MDBNavbarBrand href='/' className='py-0' style={{flex: 1}} >
                  {/* <img src={logoTikom} style={{ height: 'auto', width: '8.5rem', alignSelf: 'center', alignItems: 'center' }} alt="Logo UPTD Tikomdik" /> */}
                  <h6 className="text-center" style={{ alignItems: 'center', margin: '0.1em' }}>UPTD TIKomDik<br/>Jawa Barat</h6>
                </MDBNavbarBrand>
                <MDBCollapse id='mainNavbarCollapse' isOpen={collapseID} navbar>
                  <MDBNavbarNav right>
                    <MDBNavItem className="d-block d-md-none">
                      <MDBView
                        disabled={true}
                      >
                        <br />
                      </MDBView>
                    </MDBNavItem>
                    <MDBNavItem>
                      <MDBNavLink
                        exact
                        to='/'
                        onClick={this.closeCollapse('mainNavbarCollapse')}
                      >
                        <strong>Home</strong>
                      </MDBNavLink>
                    </MDBNavItem>
                    <MDBNavItem>
                      <MDBNavLink
                        onClick={this.closeCollapse('mainNavbarCollapse')}
                        to='/css'
                      >
                        <strong>News</strong>
                      </MDBNavLink>
                    </MDBNavItem>
                    <MDBNavItem>
                      <MDBNavLink
                        onClick={this.closeCollapse('mainNavbarCollapse')}
                        to='/components'
                      >
                        <strong>Profile</strong>
                      </MDBNavLink>
                    </MDBNavItem>
                    <MDBNavItem>
                      <MDBNavLink
                        onClick={this.closeCollapse('mainNavbarCollapse')}
                        to='/advanced'
                      >
                        <strong>Events</strong>
                      </MDBNavLink>
                    </MDBNavItem>
                    <MDBNavItem>
                      <MDBNavLink
                        onClick={this.closeCollapse('mainNavbarCollapse')}
                        to='/navigation'
                      >
                        <strong>Gallery</strong>
                      </MDBNavLink>
                    </MDBNavItem>
                    <MDBNavItem>
                      <MDBNavLink
                        onClick={this.closeCollapse('mainNavbarCollapse')}
                        to='/forms'
                      >
                        <strong>Academic</strong>
                      </MDBNavLink>
                    </MDBNavItem>
                    <MDBNavItem>
                      <MDBNavLink
                        onClick={this.closeCollapse('mainNavbarCollapse')}
                        to='/tables'
                      >
                        <strong>About</strong>
                      </MDBNavLink>
                    </MDBNavItem>
                    <MDBNavItem>
                      <MDBNavLink
                        onClick={this.closeCollapse('mainNavbarCollapse')}
                        to='/modals'
                      >
                        <strong>Our Partner</strong>
                      </MDBNavLink>
                    </MDBNavItem>
                    <MDBNavItem>
                      <MDBNavLink
                        onClick={this.closeCollapse('mainNavbarCollapse')}
                        to='/addons'
                      >
                        <strong>Account</strong>
                      </MDBNavLink>
                    </MDBNavItem>
                    {/* <MDBNavItem>
                      <MDBTooltip
                        placement='bottom'
                        domElement
                        style={{ display: 'block' }}
                      >
                        <a
                          className='nav-link Ripple-parent'
                          href='https://mdbootstrap.com/products/react-ui-kit/'
                          target='_blank'
                          rel='noopener noreferrer'
                        >
                          <strong>
                            <MDBIcon far icon='gem' />
                          </strong>
                        </a>
                        <span>PRO</span>
                      </MDBTooltip>
                    </MDBNavItem>
                    <MDBNavItem>
                      <MDBTooltip
                        placement='bottom'
                        domElement
                        style={{ display: 'block' }}
                      >
                        <a
                          className='nav-link Ripple-parent'
                          href='https://mdbootstrap.com/docs/react/getting-started/download/'
                          target='_blank'
                          rel='noopener noreferrer'
                        >
                          <strong>
                            <MDBIcon icon='download' />
                          </strong>
                        </a>
                        <span>FREE</span>
                      </MDBTooltip>
                    </MDBNavItem>
                    <MDBNavItem className='mr-2'>
                      <MDBTooltip
                        placement='bottom'
                        domElement
                        style={{ display: 'block' }}
                      >
                        <a
                          className='nav-link Ripple-parent'
                          href='https://mdbootstrap.com/support/cat/react/'
                          target='_blank'
                          rel='noopener noreferrer'
                        >
                          <strong>
                            <MDBIcon icon='question-circle' />
                          </strong>
                        </a>
                        <span>SUPPORT</span>
                      </MDBTooltip>
                    </MDBNavItem> */}
                  </MDBNavbarNav>
                </MDBCollapse>
              </MDBNavbar>
              {collapseID && overlay}
              <main style={{ marginTop: '4rem' }}>
                <Routes />
              </main>
              <MDBFooter className="pt-4 mt-4" style={{ backgroundColor: '#2d2e2f' }}>
                <MDBContainer fluid className='text-left' style={{ fontSize: 12 }}>
                  <MDBRow style={{ padding: '0.8rem' }}>
                    <MDBCol md='3'>
                      <h5 className='title'>Logo</h5>
                      <p>
                        Penjelasan Singkat UPTD Tikomdik.
                      </p><br/>
                    </MDBCol>
                    <MDBCol md='3' >
                      <div className="row">
                        <div className="col-6 col-md-12">
                          <h5 className='title'>Products</h5>
                          <ul className='list-unstyled'>
                            <li>
                              <a href='#!'>Link 1</a>
                            </li>
                            <li>
                              <a href='#!'>Link 2</a>
                            </li>
                            <li>
                              <a href='#!'>Link 3</a>
                            </li>
                            <li>
                              <a href='#!'>Link 4</a>
                            </li>
                          </ul>
                        </div>
                        <div className="col-6 col-md-12">
                          <h5 className='title'>Support</h5>
                          <ul className='list-unstyled'>
                            <li>
                              <a href='#!'>Link 1</a>
                            </li>
                            <li>
                              <a href='#!'>Link 2</a>
                            </li>
                            <li>
                              <a href='#!'>Link 3</a>
                            </li>
                            <li>
                              <a href='#!'>Link 4</a>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </MDBCol>
                    <MDBCol md='3'>
                      <div className="row">
                        <div className="col-6 col-md-12">
                          <h5 className='title'>Handy Links</h5>
                          <ul className='list-unstyled'>
                            <li>
                              <a href='#!'>Link 1</a>
                            </li>
                            <li>
                              <a href='#!'>Link 2</a>
                            </li>
                            <li>
                              <a href='#!'>Link 3</a>
                            </li>
                            <li>
                              <a href='#!'>Link 4</a>
                            </li>
                          </ul>
                        </div>
                        <div className="col-6 col-md-12">
                          <h5 className='title'>Find Us</h5>
                          <ul className='list-unstyled'>
                            <li>
                              <a href='#!'>Link 1</a>
                            </li>
                            <li>
                              <a href='#!'>Link 2</a>
                            </li>
                            <li>
                              <a href='#!'>Link 3</a>
                            </li>
                            <li>
                              <a href='#!'>Link 4</a>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </MDBCol>
                    <MDBCol md='3'>
                      <MDBView className="d-flex flex-column">
                        <h5 className='title'>About</h5>
                        <ul className='list-unstyled'>
                          <li>
                            <a href='#!'>Link 1</a>
                          </li>
                          <li>
                            <a href='#!'>Link 2</a>
                          </li>
                          <li>
                            <a href='#!'>Link 3</a>
                          </li>
                          <li>
                            <a href='#!'>Link 4</a>
                          </li>
                        </ul>
                      </MDBView>
                    </MDBCol>
                  </MDBRow>
                </MDBContainer>
              </MDBFooter>
        {/* </Sidebar> */}
            </div>
      </Router>
    );
  }
}

export default App;
